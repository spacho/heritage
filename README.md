# README #

API para obtener el patrimonio de un miembro de la familia.

El código y los tests se encuentran en la carpeta /app . En la carpeta /scripts hay un archivo de prueba de la API.

El resto del código incluido pertenece a PHPUnit y los archivos de autoload instalados mediante composer.

Se incluye también la configuración de vagrant utilizada para este desarrollo.


Repositorio:

https://bitbucket.org/spacho/heritage/