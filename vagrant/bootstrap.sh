#!/usr/bin/env bash

if [ -f /usr/bin/php ]; then
    echo "System seems already installed, exit bootstrap.sh"
    exit 0
fi

sudo add-apt-repository ppa:ondrej/php5-5.6
sudo apt-get update
sudo apt-get install -y php5

# apache modules
sudo a2enmod rewrite
sudo a2enmod expires
sudo a2enmod proxy
sudo a2enmod proxy_http

# set timezone in php.ini
sudo sed -i '/;date.timezone =/c\date.timezone = UTC' /etc/php5/apache2/php.ini
sudo sed -i '/;date.timezone =/c\date.timezone = UTC' /etc/php5/cli/php.ini

# allow short open tags
sudo sed -i '/short_open_tag = Off/c\short_open_tag = On' /etc/php5/apache2/php.ini
sudo sed -i '/short_open_tag = Off/c\short_open_tag = On' /etc/php5/cli/php.ini

# composer
sudo curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# -------------

sudo /etc/init.d/apache2 restart
