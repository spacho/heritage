<?php

require __DIR__.'/../vendor/autoload.php';

use App\Family;
use App\Member;
use App\Api;

date_default_timezone_set('Europe/Madrid');

$family = new Family();

$memberA = new Member("Alan Smith", new DateTime("1903-02-15"));

$family->addMember($memberA);

$memberB = new Member("Chris Smith", new DateTime("1930-09-23"));
$memberC = new Member("John Smith", new DateTime("1936-03-18"));

$family->addMember($memberB, $memberA);
$family->addMember($memberC, $memberA);

$memberD = new Member("Steve Smith", new DateTime("1970-11-14"));

$family->addMember($memberD, $memberC);

$memberA->addMoney(3000);
$memberA->addLands(600);
$memberA->addProperties(3);

$memberC->addMoney(300);

$currentDate = new DateTime('2016-07-15');

$name = "John Smith";
$api = new Api();
$amount = $api->getHeritageByName($name, $family, $currentDate);

echo $name . " heritage: " . $amount . "€\n";
