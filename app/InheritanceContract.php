<?php

namespace App;

interface InheritanceContract
{
    public function inheritance();
    public function resetInheritance();
}
