<?php

namespace App;
use App\Library\Inheritance;

class Member implements MemberContract, InheritanceContract
{
    public $name;
    public $dateOfBirth;

    private $_money;
    private $_lands;
    private $_properties;

    public $parent;
    public $childs;

    public $inheritedMoney;
    public $inheritedLands;
    public $inheritedProperties;

    /**
     * @param string $name Member name
     * @param \DateTime $dateOfBirth
     */
    public function __construct($name, \DateTime $dateOfBirth)
    {
        $this->name = $name;
        $this->dateOfBirth = $dateOfBirth;
        $this->_money = 0;
        $this->_lands = 0;
        $this->_properties = 0;

        $this->parent = null;
        $this->childs = array();
    }

    /**
     * @param MemberContract $parent
     */
    public function setParent(MemberContract $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @param MemberContract $child
     */
    public function addChild(MemberContract $child)
    {
        $this->childs[] = $child;
    }

    /**
     * @param \DateTime $currentDate
     *
     * @return int Age at current date
     */
    public function age(\DateTime $currentDate)
    {
        $diff = $currentDate->diff($this->dateOfBirth);
        return $diff->y;
    }

    /**
     * @param \DateTime $currentDate
     *
     * @return int Days since birth to current date
     */
    public function days(\DateTime $currentDate)
    {
        $diff = $currentDate->diff($this->dateOfBirth);
        return $diff->days;
    }

    /**
     * @param \DateTime $currentDate
     *
     * @return bool Whether if member is alive at current date or not
     */
    public function isAlive(\DateTime $currentDate)
    {
        if ($this->age($currentDate) >= 100) {
            return false;
        }

        return true;
    }

    /**
     * @param int $amount
     */
    public function addMoney($amount)
    {
        $this->_money += $amount;
    }

    /**
     * @param int $squareMeters
     */
    public function addLands($squareMeters)
    {
        $this->_lands += $squareMeters;
    }

    /**
     * @param int $number
     */
    public function addProperties($number)
    {
        $this->_properties += $number;
    }

    /**
     * @return int $money
     */
    public function getMoney()
    {
        return $this->_money;
    }

    /**
     * @return int $lands
     */
    public function getLands()
    {
        return $this->_lands;
    }

    /**
     * @return int $properties
     */
    public function getProperties()
    {
        return $this->_properties;
    }

    /**
     * @param \DateTime $currentDate
     *
     * @return int Total member's amount at current date
     */
    public function heritage(\DateTime $currentDate)
    {
        if ($this->parent) {
            $this->parent->heritage($currentDate);
        }

        if ($this->age($currentDate) >= 100) {
            Inheritance::distribute($this, $currentDate);
        }

        return $this->_money + $this->_lands * MemberContract::LANDS_VALUE + $this->_properties * MemberContract::PROPERTIES_VALUE + $this->inheritance();
    }

    /**
     * @return int Total member's inherited amount
     */
    public function inheritance()
    {
        return $this->inheritedMoney + $this->inheritedLands * MemberContract::LANDS_VALUE + $this->inheritedProperties * MemberContract::PROPERTIES_VALUE;
    }

    public function resetInheritance()
    {
        $this->inheritedMoney = 0;
        $this->inheritedLands = 0;
        $this->inheritedProperties = 0;
    }
}
