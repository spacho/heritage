<?php

namespace App;

interface MemberContract
{
    const LANDS_VALUE = 300;
    const PROPERTIES_VALUE = 1000000;

    public function age(\DateTime $currentDate);
    public function days(\DateTime $currentDate);
    public function isAlive(\DateTime $currentDate);
    public function addMoney($amount);
    public function addLands($squareMeters);
    public function addProperties($number);
    public function getMoney();
    public function getLands();
    public function getProperties();
    public function heritage(\DateTime $currentDate);
}
