<?php

namespace App\Library;
use App\MemberContract;

abstract class Inheritance
{
    /**
     * @param MemberContract $member Member that receives the inheritance
     * @param \DateTime $currentDate
     */
    public static function distribute(MemberContract $member, \DateTime $currentDate)
    {
        self::_distributeMoney($member, $member->getMoney(), true);
        self::_distributeLands($member, $member->getLands(), $currentDate, true);
        self::_distributeProperties($member, $member->getProperties(), $currentDate, true);
    }

    /**
     * @param MemberContract $member Member that receives the inheritance
     * @param int $money Amount of money to distribute
     */
    private static function _distributeMoney(MemberContract $member, $money, $legator = false)
    {
        if (empty($member->childs)) {
            if (!$legator) {
                $member->inheritedMoney += $money;
            }
        } else {
            if (!$legator) {
                $selfMoney = round($money / 2);
                $member->inheritedMoney += $selfMoney;
            } else {
                $selfMoney = 0;
            }
            $childsMoney = $money - $selfMoney;
            $remaining = $childsMoney;
            $childsNumber = count($member->childs);

            foreach ($member->childs as $child) {
                $childMoney = round($childsMoney / $childsNumber);
                if ($childMoney > $remaining) {
                    $childMoney = $remaining;
                }
                self::_distributeMoney($child, $childMoney);
                $remaining -= $childMoney;
            }
        }
    }

    /**
     * @param MemberContract $member Member that receives the inheritance
     * @param int $lands Number of lands to distribute
     * @param \DateTime $currentDate
     */
    private static function _distributeLands(MemberContract $member, $lands, \DateTime $currentDate)
    {
        if (empty($member->childs)) {
            return;
        }

        $heir = $member->childs[0];
        foreach ($member->childs as $child) {
            if ($heir->name == $child->name) {
                continue;
            }

            if ($child->days($currentDate) > $heir->days($currentDate)) {
                $heir = $child;
            } elseif ($child->days($currentDate) == $heir->days($currentDate)) {
                if (strcmp($child->name, $heir->name) < 0) {
                    $heir = $child;
                }
            }
        }

        $heir->inheritedLands += $lands;
    }

    /**
     * @param MemberContract $member Member that receives the inheritance
     * @param int $properties Number of properties to distribute
     * @param \DateTime $currentDate
     */
    private static function _distributeProperties(MemberContract $member, $properties, \DateTime $currentDate)
    {
        if (empty($member->childs)) {
            return;
        }

        usort($member->childs, function($a, $b) use($currentDate) {
            if ($a->days($currentDate) == $b->days($currentDate)) {
                return 0;
            }

            return ($a->days($currentDate) < $b->days($currentDate)) ? -1 : 1;
        });

        $childsNumber = count($member->childs);
        $counter = 0;

        for ($i = 0; $i < $properties; $i++) {
            $child = $member->childs[$counter];
            $child->inheritedProperties++;

            if ($counter + 1 == $childsNumber) {
                $member->childs = array_reverse($member->childs);
                $counter = 0;
            } else {
                $counter++;
            }
        }
    }
}
