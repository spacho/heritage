<?php

namespace App;

class Family
{
    private $_names;
    private $_members;
    private $_heritages;

    public function __construct()
    {
        $this->_names = array();
        $this->_members = array();
    }

    /**
     * @param MemberContract $member
     * @param MemberContract $parent
     */
    public function addMember(MemberContract $member, MemberContract $parent = null)
    {
        if ($this->_memberExists($member->name)) {
            throw new \Exception('A member named ' . $member->name . ' already exists!');
        }

        if ($parent && !$this->_memberExists($parent->name)) {
            throw new \Exception('A member named ' . $parent->name . ' does not exist!');
        }

        if ($parent && $member->dateOfBirth <= $parent->dateOfBirth) {
            throw new \Exception('Parent ' . $parent->name . ' must be older than child ' . $member->name . '!');
        }

        $this->_names[] = $member->name;
        $memberId = $this->_getMemberId($member->name);
        $this->_members[$memberId] = $member;

        if ($parent) {
            $parentMember = $this->getMemberByName($parent->name);
            $this->_setRelationships($this->_members[$memberId], $parentMember);
        }
    }

    /**
     * @param string $name Member name
     *
     * @return Member Member object if found, null if not
     */
    public function getMemberByName($name)
    {
        $memberId = $this->_getMemberId($name);
        return is_numeric($memberId) ? $this->_members[$memberId] : null;
    }

    /**
     * @param string $name Member name
     *
     * @return bool Whether if member exists or not
     */
    private function _memberExists($name)
    {
        if (in_array($name, $this->_names)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $name Member name
     *
     * @return int Member id, null if not found
     */
    private function _getMemberId($name)
    {
        return array_search($name, $this->_names);
    }

    /**
     * @param MemberContract $member
     * @param MemberContract $parent
     */
    private function _setRelationships(MemberContract $member, MemberContract $parent)
    {
        if ($parent) {
            $member->setParent($parent);
        }

        $parent->addChild($member);
    }
}
