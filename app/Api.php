<?php

namespace App;

class Api
{
    /**
     * @param string $name Member name
     * @param Famiy $family Family structure
     * @param \DateTime $currentDate
     *
     * @return int Total member's amount at current date, null if not found
     */
    public function getHeritageByName($name, Family $family, $currentDate)
    {
        $member = $family->getMemberByName($name);

        if (!$member) {
            return null;
        } elseif (!$member->isAlive($currentDate)) {
            return 0;
        } else {
            $member->resetInheritance();
            return $member->heritage($currentDate);
        }
    }
}
