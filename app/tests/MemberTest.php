<?php
use PHPUnit\Framework\TestCase;
use App\Member;

class MemberTest extends TestCase
{
    public function testAge()
    {
        $member = new Member('Marco Van Basten', new \DateTime('1964-10-31'));
        $this->assertEquals(51, $member->age(new \DateTime('2016-07-15')));
    }

    public function testDays()
    {
        $member = new Member('Wayne Rooney', new \DateTime('1985-10-24'));
        $this->assertEquals(8880, $member->days(new \DateTime('2010-02-15')));
    }

    public function testAlive()
    {
        $member = new Member('Albert Einstein', new \DateTime('1879-03-14'));
        $this->assertFalse($member->isAlive(new \DateTime('2001-09-23')));

        $member = new Member('Paolo Maldini', new \DateTime('1968-06-26'));
        $this->assertTrue($member->isAlive(new \DateTime('2016-02-18')));
    }

    public function testHeritage()
    {
        $member = new Member('Flavio Briatore', new \DateTime('1950-04-12'));
        $member->addMoney(10000000);
        $member->addLands(30);
        $member->addProperties(5);
        $this->assertEquals(15009000, $member->heritage(new \DateTime('2010-12-31')));
    }
}
