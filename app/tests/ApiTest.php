<?php
use PHPUnit\Framework\TestCase;
use App\Api;
use App\Family;
use App\Member;

class ApiTest extends TestCase
{
    public function testHeritage()
    {
        $api = new Api();

        $family = new Family();

        $memberA = new Member('Tycos Lannister', new \DateTime('1916-07-14'));
        $memberB = new Member('Tywin Lannister', new \DateTime('1940-02-27'));
        $memberC = new Member('Kevan Lannister', new \DateTime('1941-03-12'));

        $memberD = new Member('Cersei Lannister', new \DateTime('1962-05-19'));
        $memberE = new Member('Jaime Lannister', new \DateTime('1963-02-08'));
        $memberF = new Member('Tyrion Lannister', new \DateTime('1965-06-21'));

        $memberG = new Member('Lancel Lannister', new \DateTime('1965-02-14'));
        $memberH = new Member('Martyn Lannister', new \DateTime('1967-09-03'));

        $memberI = new Member('Joffrey Baratheon', new \DateTime('1975-09-02'));
        $memberJ = new Member('Tommen Baratheon', new \DateTime('1978-02-25'));

        $family->addMember($memberA);

        $family->addMember($memberB, $memberA);
        $family->addMember($memberC, $memberA);

        $family->addMember($memberD, $memberB);
        $family->addMember($memberE, $memberB);
        $family->addMember($memberF, $memberB);

        $family->addMember($memberG, $memberC);
        $family->addMember($memberH, $memberC);

        $family->addMember($memberI, $memberD);
        $family->addMember($memberJ, $memberD);

        $memberA->addMoney(100000);

        $currentDate = new \DateTime('2016-07-15');
        $this->assertEquals(25000, $api->getHeritageByName('Tywin Lannister', $family, $currentDate));
        $this->assertEquals(25000, $api->getHeritageByName('Kevan Lannister', $family, $currentDate));
        $this->assertEquals(4167, $api->getHeritageByName('Cersei Lannister', $family, $currentDate));
        $this->assertEquals(8333, $api->getHeritageByName('Jaime Lannister', $family, $currentDate));
        $this->assertEquals(8333, $api->getHeritageByName('Tyrion Lannister', $family, $currentDate));
        $this->assertEquals(12500, $api->getHeritageByName('Lancel Lannister', $family, $currentDate));
        $this->assertEquals(12500, $api->getHeritageByName('Martyn Lannister', $family, $currentDate));
        $this->assertEquals(2083, $api->getHeritageByName('Joffrey Baratheon', $family, $currentDate));
        $this->assertEquals(2083, $api->getHeritageByName('Tommen Baratheon', $family, $currentDate));
    }
}
