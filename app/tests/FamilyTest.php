<?php
use PHPUnit\Framework\TestCase;
use App\Family;
use App\Member;

class FamilyTest extends TestCase
{
    public function testParent()
    {
        $family = new Family();

        $memberA = new Member('Phil Collins', new \DateTime('1951-01-30'));
        $memberB = new Member('Simon Collins', new \DateTime('1976-09-14'));

        $family->addMember($memberA);
        $family->addMember($memberB, $memberA);

        $this->assertEquals('Phil Collins', $memberB->parent->name);
    }

    public function testChilds()
    {
        $family = new Family();

        $memberA = new Member('Donald Sutherland', new \DateTime('1935-07-17'));
        $memberB = new Member('Kiefer Sutherland', new \DateTime('1966-12-21'));
        $memberC = new Member('Sarah Sutherland', new \DateTime('1988-02-18'));

        $family->addMember($memberA);
        $family->addMember($memberB, $memberA);
        $family->addMember($memberC, $memberB);

        $this->assertContains($memberB, $memberA->childs);
        $this->assertContains($memberC, $memberB->childs);
    }
}
